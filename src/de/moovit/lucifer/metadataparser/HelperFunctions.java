package de.moovit.lucifer.metadataparser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class HelperFunctions {
	private final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(HelperFunctions.class);
	
	
	private void iterateTroughFolder(File folder) throws IOException, XPathException {
		for(File file : folder.listFiles()) {
			if(!file.getAbsolutePath().contains("members")) {
				String xml = readFile(file.getAbsolutePath());
				logger.info("Tag Count: " + countXmlTags(xml) + " of xml file " + file.getName());
			}
		}
	}

	private String readFile(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally {
			br.close();
		}
	}

	private int countXmlTags(String xml) throws XPathException {
		String xpathExpr = "//*";
		XPathFactory factory = XPathFactory.newInstance();
		XPath xPath = factory.newXPath();
		NodeList nodeList = (NodeList) xPath.evaluate(xpathExpr, new InputSource(new StringReader(xml)),
				XPathConstants.NODESET);
		return nodeList.getLength();
	}

	public void checkDescType(File folder) throws IOException {
		String line;

		for (File file : folder.listFiles()) {
			if (file.getAbsolutePath().contains("members")) {
				try {
					BufferedReader br = new BufferedReader(new FileReader(file));
					while ((line = br.readLine()) != null) {
						if (line.contains("DESC_TYPE")) {
							logger.info(br.readLine());
						}
					}
					br.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
