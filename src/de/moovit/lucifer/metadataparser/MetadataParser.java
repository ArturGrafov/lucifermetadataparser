package de.moovit.lucifer.metadataparser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;

public class MetadataParser {
	private final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(MetadataParser.class);

	private List<AssetMetadata> assetMetadataList = new ArrayList<>();
	private List<String> counterInfo = new ArrayList<>();
	private final String xml10pattern = "[^" + "\u0009\r\n" + "\u0020-\uD7FF" + "\uE000-\uFFFD"
			+ "\ud800\udc00-\udbff\udfff" + "]";

	public MetadataParser() {
	}

	public List<AssetMetadata> getAssetMetadataList() {
		return assetMetadataList;
	}

	public void setAssetMetadataList(List<AssetMetadata> assetMetadataList) {
		this.assetMetadataList = assetMetadataList;
	}

	public void parseXml(String path) {
		int counter = 0;
		
		File xml = new File(path);
		String name = xml.getName().split("\\.")[0];
		File xmlNew = new File(xml.getParent() + name + "_new.xml");

		String line;

		// Replace invalid chars and xml rules.
		try {
			BufferedReader br = new BufferedReader(new FileReader(xml));
			BufferedWriter bw = new BufferedWriter(new FileWriter(xmlNew));
			while ((line = br.readLine()) != null) {
				line = line.replaceAll(xml10pattern, "");
				line = line.replaceAll("&#13;", " ");
				line = line.replaceAll("&quot;", "\"");
				line = line.replaceAll("&amp;", "");
				line = line.replaceAll("&lt;", "");
				line = line.replaceAll("&gt;", "");
				line = line.replaceAll("&apos;", "");
				line = line.replaceAll("&#xFC;", "UE");
				bw.write(line);
			}
			br.close();
			bw.close();
		} catch (Exception e) {
			logger.debug(e);
			logger.error(e.getMessage());
		}

		try {
			XMLInputFactory xmlif = XMLInputFactory.newInstance();
			FileReader fr = new FileReader(xmlNew.getAbsolutePath());
			XMLEventReader xmler = xmlif.createXMLEventReader(fr);

			while (xmler.peek() != null) {
				AssetMetadata assetMetadata = new AssetMetadata();
				assetMetadata.setAssetId(name);
				try {
					Object id;
					if (!((id = xmler.nextEvent()).toString().contains("\n"))) {
						if (id.toString().contains("id=")) {
							Pattern p = Pattern.compile("\\'(.*?)\\'");
							Matcher m = p.matcher(id.toString());
							while (m.find()) {
								assetMetadata.setName(m.group(1));
							}
							Object value = xmler.nextEvent(); // jump over \n
							value = xmler.nextEvent(); // jump over next tag
							value = xmler.nextEvent(); // value entry if value not empty, else closing tag
							if (!value.toString().contains("</")) { // skip if closing tag
								assetMetadata.setValue(value.toString());
							}
							//Check for null name -> appears only if xmler.nextEvent(); tries to read speacial chars
							if(assetMetadata.getName() == null) {
								logger.info("null name in: " + assetMetadata.getAssetId());
								AssetMetadata asMeta = this.assetMetadataList.get(this.assetMetadataList.size()-1);
								String lastId = asMeta.getAssetId();
								String lastName = asMeta.getName();
								String lastValue = asMeta.getValue();
								logger.info("last entry: " + lastId + "\t" + lastName + "\t" + lastValue);
							}
							this.assetMetadataList.add(assetMetadata);
							counter ++;
						}
					}
				} catch (Exception e) {
					logger.error(e);
					break;
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}
		xmlNew.delete();
//		logger.info(counter + "  " + name);
	}

	// string input as lower case
	public void searchValueForString(String string) {
		for (AssetMetadata assetMetadata : this.assetMetadataList) {
			if (assetMetadata.getValue() != null) {
				if (assetMetadata.getValue().toLowerCase().contains(string)) {
					logger.info(assetMetadata.getName() + "     " + assetMetadata.getValue());
				}
			}
		}
	}
	

	// string input as lower case
	public void searchIDForString(String string) {
		for (AssetMetadata assetMetadata : this.assetMetadataList) {
			try {
				if (assetMetadata.getName().toLowerCase().contains(string)) {
					logger.info(assetMetadata.getName());
				}
			} catch (Exception e) {
				logger.info(assetMetadata.getName());
			}

		}
	}

	public void printAssetMetadataListToFile(String path) {
		File outputPath = new File(path);
		if (!assetMetadataList.isEmpty()) {
			BufferedWriter br;
			try {
				br = new BufferedWriter(new FileWriter(outputPath));
				for (AssetMetadata assetMetadata : this.assetMetadataList) {
					br.write(assetMetadata.getAssetId() + "  " + assetMetadata.getName() + "  "
							+ assetMetadata.getValue() + "\n");
				}
				br.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

}

