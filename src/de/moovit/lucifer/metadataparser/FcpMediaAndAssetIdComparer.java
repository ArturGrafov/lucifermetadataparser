package de.moovit.lucifer.metadataparser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FcpMediaAndAssetIdComparer{
	private final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FcpMediaAndAssetIdComparer.class);

	public List<String> readFolderRecursive(String path) {
		Stream<Path> files;
		try {
			files = Files.walk(Paths.get(path)).filter(Files::isRegularFile);
			List<Path> temp = files.collect(Collectors.toList());
			List<String> fileList = temp.stream().map(p -> p.getFileName().toString().split("\\.")[0].split("_")[0]).collect(Collectors.toList());
			return fileList;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void compare(List<String> assets, List <String> fcp) {
		for(String media : fcp) {
			if(!assets.contains(media)) {
				logger.info("FCP media not inside Asset folder: " + media) ;
			}
		}
	}
}
