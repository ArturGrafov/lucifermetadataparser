package de.moovit.lucifer.metadataparser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

public class XmlWriter {
	private final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XmlWriter.class);

	private String mediaId;
	private String title;
	private String description;
	private String createdBy;
	private String created;
	private String fileName;
	private String category;
	private String creationDate;
	private String duration;
	private String videoFormatFilingMeta;
	private String videoFormatNonRealTime;
	private String audioFormat;
	private String rights;
	private String frameRate;
	private List<String> customMetadata = new ArrayList<>();

	public String getMediaId() {
		return this.mediaId;
	}

	public void consolidateMetadataPerAsset(AssetMetadata assetMeta, String outputPath) throws Exception {

		String fcsId = assetMeta.getAssetId().toString();
		String hiveId = this.mediaId;
		boolean equal = (fcsId.equals(hiveId)) ? true : false;
		// Check if it is an assetMetadata object of a new Asset.
		if (!equal && this.mediaId != null) {
			writeXml(outputPath + hiveId + "_forImport.xml");
			resetAllEntries();
		}

		this.mediaId = assetMeta.getAssetId();

		switch (assetMeta.getName()) {
		case "ASSET_NUMBER":
			this.mediaId = assetMeta.getValue();
			break;

		case "PA_MD_CUST_TITEL":
			this.title = assetMeta.getValue();
			break;

		case "CUST_CATEGORY":
			this.category = assetMeta.getValue();
			break;

		case "CUST_CREATED":
			this.creationDate = assetMeta.getValue();
			break;

		case "ENTITY_CREATE_USER_NAME":
			this.createdBy = assetMeta.getValue();
			break;

		case "PA_MD_CUST_RECHTELAGE":
			this.rights = assetMeta.getValue();
			break;

		case "ASSET_VIDEO_CODEC":
			this.videoFormatNonRealTime = assetMeta.getValue();
			break;

		case "ASSET_VIDEO_FRAME_RATE":
			this.frameRate = assetMeta.getValue();
			break;

		case "ASSET_DURATION":
			this.duration = assetMeta.getValue();
			break;

		case "CUST_DESCRIPTION":
			this.description = assetMeta.getValue();
			break;

		case "CUST_TITLE":
			this.fileName = assetMeta.getValue();
			break;

		case "FILE_CREATE_DATE":
			this.created = assetMeta.getValue();
			break;

		case "ASSET_IMAGE_SIZE":
			this.videoFormatFilingMeta = assetMeta.getValue();
			break;

		case "ASSET_AUDIO_CODEC":
			this.audioFormat = assetMeta.getValue();
			break;

		default:
			if (assetMeta.getValue() != null) {
				// The .replace("_?", "") is for tag PA_MD_CUST_FINAL_CUT_7_PROJEKT_? which
				// causes an error.
				this.customMetadata.add("<" + assetMeta.getName().replace("_?", "") + "_CHAR datatype=\"string\"" + ">"
						+ assetMeta.getValue() + "</" + assetMeta.getName().replace("_?", "") + "_CHAR>");
			}
			break;
		}
	}

	public void resetAllEntries() {
		this.mediaId = null;
		this.title = null;
		this.description = null;
		this.createdBy = null;
		this.created = null;
		this.fileName = null;
		this.category = null;
		this.creationDate = null;
		this.duration = null;
		this.videoFormatNonRealTime = null;
		this.audioFormat = null;
		this.rights = null;
		this.videoFormatFilingMeta = null;
		this.frameRate = null;
		this.customMetadata.clear();
	}

	public void writeXml(String outputPath) throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		File hiveXmlSchema = new File(classLoader.getResource("HIVE_xmlSchema.xml").getFile());

		try {
			BufferedReader br = new BufferedReader(new FileReader(hiveXmlSchema));
			BufferedWriter bw = new BufferedWriter(new FileWriter(outputPath));
			String line;
			while ((line = br.readLine()) != null) {
				if (!line.contains("xml version")) {
					line = line.replaceAll(" ", "");
				}

				if (line.contains("_MediaID")) {
					line = "<MediaID>" + (this.mediaId == null ? "" : this.mediaId) + "</MediaID>";
				} else if (line.contains("_Title")) {
					line = "<Title>" + (this.title == null ? "" : this.title) + "</Title>";
				} else if (line.contains("_Description")) {
					line = "<description>" + (this.description == null ? "" : this.description) + "</description>";
				} else if (line.contains("_createdBy")) {
					line = "<createdBy>" + (this.createdBy == null ? "" : this.createdBy) + "</createdBy>";
				} else if (line.contains("_created")) {
					line = "<created>" + (this.created == null ? "" : this.created) + "</created>";
				} else if (line.contains("_FileName")) {
					line = "<FileName>" + (this.fileName == null ? "" : this.fileName) + "</FileName>";
				} else if (line.contains("_Category")) {
					line = "<Category>" + (this.category == null ? "" : this.category) + "</Category>";
				} else if (line.contains("_CreationDate")) {
					line = "<CreationDate>" + (this.creationDate == null ? "" : this.creationDate) + "</CreationDate>";
				} else if (line.contains("_Duration")) {
					line = "<Duration>" + (this.duration == null ? "" : this.duration) + "</Duration>";
				} else if (line.contains("_VideoFormatNonRealTime")) {
					line = "<VideoFormat>" + (this.videoFormatNonRealTime == null ? "" : this.videoFormatNonRealTime)
							+ "</VideoFormat>";
				} else if (line.contains("_AudioFormat")) {
					line = "<AudioFormat>" + (this.audioFormat == null ? "" : this.audioFormat) + "</AudioFormat>";
				} else if (line.contains("_Description")) {
					line = "<Description>" + (this.description == null ? "" : this.description) + "</Description>";
				} else if (line.contains("_Rights")) {
					line = "<Rights>" + (this.rights == null ? "" : this.rights) + "</Rights>";
				} else if (line.contains("_VideoFormatFilingMeta")) {
					line = "<VideoFormat>" + (this.videoFormatFilingMeta == null ? "" : this.videoFormatFilingMeta)
							+ "</VideoFormat>";
				} else if (line.contains("_AudioFormat")) {
					line = "<AudioFormat>" + (this.audioFormat == null ? "" : this.audioFormat) + "</AudioFormat>";
				} else if (line.contains("_Framerate")) {
					line = "<Framerate>" + (this.frameRate == null ? "" : this.frameRate) + "</Framerate>";
				} else if (line.contains("_Custom")) {
					line = "<Custom>";
					for (int i = 0; i < this.customMetadata.size(); i++) {
						line = line.concat(customMetadata.get(i));
					}
					line = line.concat("</Custom>");
				} else {
					logger.info("Data mapping failed.");
				}
				bw.write(line);
			}
			bw.close();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// prettyPrint xml
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new File(outputPath));

		Transformer tform = TransformerFactory.newInstance().newTransformer();
		tform.setOutputProperty(OutputKeys.INDENT, "yes");
		tform.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		tform.transform(new DOMSource(document), new StreamResult(new File(outputPath)));
	}
}
