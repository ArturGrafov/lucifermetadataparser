package de.moovit.lucifer.metadataparser;

import java.io.File;
import java.util.List;

public class Main {

	public static void main(String[] args) throws Exception {
		
		File folder = new File("/Users/artur/Desktop/Sony/FCS1/fcsvr_export/ASSET_XML");	//fcs1
//		File folder = new File("/Users/artur/Desktop/Sony/FCS2/fcsvr_export/ASSET_XML"); 	//fcs2
//		File folder = new File("/Users/artur/Desktop/Sony/FCS3/fcsvr_export/ASSET_XML");	//fcs3
//		File folder = new File("/Users/artur/Desktop/Sony/FCS4/fcsvr_export/ASSET_XML"); 	//fcs4
//		File folder = new File("/Users/artur/Desktop/Sony/Testcases/writeXml");

//		List<AssetMetadata> metadataList = doParsing(folder);
//		writeXml(metadataList, folder + "/forImport/");
		
		
//		collectAllCustomTags(folder);

	}


//	private static void collectAllCustomTags(File folder) {
//		XmlCustomTagCollector xmlCustomTagCollector = new XmlCustomTagCollector();
//		
//		
//		
//	}


	private static List<AssetMetadata> doParsing(File folder) {
		MetadataParser metadataParser = new MetadataParser();
		int numberOfFiles = folder.listFiles().length / 2;
		System.out.println("Number of files: " + numberOfFiles);
		int counter = 0;
		int progress = 0;

		System.out.println("Reading data...");
		for (File file : folder.listFiles()) {
			// Change the ! to get the other half of the xml files.
			if (!file.getName().contains("members")) {
				metadataParser.parseXml(file.getAbsolutePath());
				int progressOld = progress;
				counter++;
				if (counter % 100 == 0) {
					progress = counter * 100 / numberOfFiles;
					if (progress != progressOld) {
						System.out.println(progress + "%");
					}
				}
			}
		}
		System.out.println("Parsing completed.");
		return metadataParser.getAssetMetadataList();
	}

	private static void writeXml(List<AssetMetadata> metadataList, String outputPath) throws Exception {
		System.out.println("Writing xml files...");
		new File(outputPath).mkdir();
		XmlWriter xmlWriter = new XmlWriter();
		
		int counter = 0;
		int progress = 0;
		
		for(AssetMetadata assetMeta : metadataList) {
			xmlWriter.consolidateMetadataPerAsset(assetMeta, outputPath);
			int progressOld = progress;
			counter++;
			if (counter % 100 == 0) {
				progress = counter * 100 / metadataList.size();
				if (progress != progressOld) {
					System.out.println(progress + "%");
				}
			}
		}
		// For last handled file the if condition is not checked correctly since there will be no mediaId change after last file.
		xmlWriter.writeXml(outputPath + xmlWriter.getMediaId() + "_forImport.xml");
		xmlWriter.resetAllEntries();
		System.out.println("Process completed.");
	}
}
